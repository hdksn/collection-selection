package com.hdksn.selection

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.RepeatedTest
import org.junit.jupiter.api.Test

internal class SelectionKtTest {

    @Test
    fun `must select 0 or more elements`() {
        assertThrows(IllegalArgumentException::class.java) {
            listOf(1).randomSelection(-1)
        }
    }

    @Test
    fun `accepts empty lists if asking for 0 elements`() {
        assertTrue(
            emptyList<Int>().randomSelection(0).isEmpty())
    }

    @Test
    fun `rejects empty lists if asking for elements`() {
        assertThrows(IllegalArgumentException::class.java) {
            emptyList<Int>().randomSelection(1)
        }
    }

    @Test
    fun `count doesn't matter if duplicates allowed`() {
        assertEquals(List(5) {1},
            listOf(1).randomSelection(5, duplicates = true))
    }

    @Test
    fun `validates list size with no duplicates`() {
        assertThrows(IllegalArgumentException::class.java) {
            listOf(1,2).randomSelection(3)
        }
    }

    @Test
    fun `selects from all elements randomly if duplicates allowed`() {
        val expectedRange = 70 until 130
        val selections = listOf(1,2,3).randomSelection(300, duplicates = true)
        assertTrue(selections.count { it == 1 } in expectedRange)
        assertTrue(selections.count { it == 2 } in expectedRange)
        assertTrue(selections.count { it == 3 } in expectedRange)
    }

    @Test
    fun `selects all elements if count is the same as the list size`() {
        val listSize = 100
        val startingList = List(listSize){ it }
        val selections = startingList.randomSelection(listSize)
        assertEquals(startingList.toSet(), selections.toSet())
    }

    @Test
    fun `selection is optimised if selecting fewer than 10% of the list elements`() {
        val listSize = 1000
        val startingList = List(listSize){ it }
        val selections = startingList.randomSelection(50)
        assertEquals(50, selections.toSet().size)
    }

    @RepeatedTest(10)
    fun `selects from all elements randomly if duplicates not allowed`() {
        val counts = mutableMapOf<Int,Int>()
        val expectedRange = 30 until 70
        val list = listOf(1,2,3,4,5,6,7,8,9,10)
        repeat(100) {
            val results = list.randomSelection(5)
            assertEquals(5, results.toSet().size)
            results.forEach { chosen ->
                val count = counts[chosen] ?: 0
                counts[chosen] = count + 1
            }
        }
        counts.forEach { (k,v) ->
            assertTrue(v in expectedRange, "$v was not in $expectedRange for value $k")
        }
    }
}