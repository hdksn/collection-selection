package com.hdksn.selection

import java.security.SecureRandom
import kotlin.random.Random
import kotlin.random.asKotlinRandom

/** Returns a random selection of elements, by default never returning elements from the same index */
fun <T> List<T>.randomSelection(
    count: Int,
    duplicates: Boolean = false,
    random: Random = SecureRandom().asKotlinRandom()
): List<T> {
    require(count >= 0) { "count must be >= 0" }

    return when {
        count == 0 -> emptyList()
        duplicates -> listWithDuplicates(count, random)
        else -> listNoDuplicates(count, random)
    }
}

private fun <T> List<T>.listWithDuplicates(count: Int, random: Random): List<T> {
    return List(count) { this[random.nextInt(size)] }
}

private fun <T> List<T>.listNoDuplicates(count: Int, random: Random): List<T> {
    require(count <= size) { "Can't select $count unique elements from list only containing $size elements" }

    if (size * 0.1 < count) {
        return shuffled(random).take(count)
    }

    val swapped = mutableMapOf<Int, Int>()

    return List(count) { i ->
        val swapWith = random.nextInt(i, size)

        val alreadySwapped = swapped[swapWith]
        val existingSwap = swapped[i] ?: i
        swapped[swapWith] = existingSwap

        if (alreadySwapped == null) {
            this[swapWith]
        } else {
            this[alreadySwapped]
        }
    }
}


